# Swift 

Сервис сбора и обработки событий статистики


### Install

Для работы приложения требуется установить *go* версии 1.7 и выше:
```sh
$ brew install go
```

Создаём директорию и устанавливаем переменную окружения
```sh
mkdir ~/golib
export GOPATH=~/golib
export PATH=$PATH:$GOPATH/bin
```

### Устанавливаем gb

```
go get -u github.com/constabulary/gb/...
```
Подробнее тут https://getgb.io/docs/install/

### Компиляция и запуск приложения

```sh
$ cd <project>
$ gb vendor restore
$ gb build
$ cp config.ini.example config.ini
$ ./bin/swift -verbose
```


Имитируем срабатывание кода счётчика на странице:
```sh
http localhost:9034/boom referer=='http://yandex.ru/news/123' categories=='{"rubric":"business","page":"document"}' user_options=='{"user_id":123,"subscribe":1,"auth":"fb"}' event=='click' view=='iphone' ip=='8.8.8.8' Referer:http://yandex.ru/ --session=user
```
