package main

import (
	"strings"
	"testing"
)

var rawUrlTestCases = [...]struct {
	url string
	enc string
}{
	{url: "http://vedomosti.ru/", enc: "http://vedomosti.ru/"},
	{url: "http://vedomosti.ru/?q=Поиск", enc: "http://vedomosti.ru/?q=%D0%9F%D0%BE%D0%B8%D1%81%D0%BA"},
	{url: "http://нигма.рф/", enc: "http://xn--80aforc.xn--p1ai/"},
	{url: "", enc: ""},
}

func TestFormatUrl(t *testing.T) {
	for _, tc := range rawUrlTestCases {
		formated_url, err := formatUrl(tc.url)
		if err != nil {
			t.Errorf(`Error parse %q: %q %s`, tc.url, formated_url, err)
		} else if formated_url != tc.enc {
			t.Errorf(`got: %q, want: %q`, formated_url, tc.enc)
		}
	}

	invalid_url := "http://vedomosti.ru/news" + strings.Repeat("ab", 2049)
	_, err := formatUrl(invalid_url)
	if err == nil || err.Error() != "Invalid length url" {
		t.Errorf(`Error parse %q %s`, invalid_url, err)
	}
}
