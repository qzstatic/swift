package main

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/garyburd/redigo/redis"
	"github.com/stretchr/testify/assert"
	"github.com/vedomosti/swift/svc"
	"github.com/vedomosti/swift/visit"
)

func TestRegisterVisit(t *testing.T) {
	config := svc.ParseFlags()
	resources := svc.NewResources(config)
	resources.ConnectRedis()
	manager := NewManager(resources)

	r := resources.Redis()
	defer r.Close()
	r.Do("FLUSHALL")

	uri := `/boom?referer=http://news.yandex.ru` +
		`&tags=["rubric:russia","page:topic","user_id:123","auth:fb"]&view=iphone`

	rw, req := GetRWReq(uri, "http://www.vedomosti.ru/news/")

	manager.HandlerRegisterVisit(rw, req)

	assert.Equal(t, 200, rw.Code)

	jsd, err := redis.Bytes(r.Do("RPOP", "visits"))
	assert.Nil(t, err)

	v := &visit.Visit{RawVisit: jsd}
	err = v.Parse()
	assert.Nil(t, err)

	assert.Equal(t, "0d632ee9-121e-4e6e-8997-fa031996becd", v.Swuuid)
	assert.Equal(t, "1.2.3.4", v.Ip)
}

func TestRedirect(t *testing.T) {
	config := svc.ParseFlags()
	resources := svc.NewResources(config)
	resources.ConnectRedis()
	manager := NewManager(resources)

	r := resources.Redis()
	defer r.Close()
	r.Do("FLUSHALL")

	uri := `/redirect?url=http://product.info&referer=editor_choice_20151020` +
		`&tags=["contact_id:123"]`

	rw, req := GetRWReq(uri, "")

	manager.HandlerRedirect(rw, req)

	assert.Equal(t, 302, rw.Code)
	assert.Equal(t, "http://product.info", rw.Header().Get("Location"))

	jsd, err := redis.Bytes(r.Do("RPOP", "visits"))
	assert.Nil(t, err)

	v := &visit.Visit{RawVisit: jsd}
	err = v.Parse()
	assert.Nil(t, err)

	assert.Equal(t, "0d632ee9-121e-4e6e-8997-fa031996becd", v.Swuuid)
	assert.Equal(t, "1.2.3.4", v.Ip)

	// when url is empty
	uri = `/redirect?referer=editor_choice_20151020` +
		`&tags=["contact_id:123"]`

	rw, req = GetRWReq(uri, "")
	manager.HandlerRedirect(rw, req)

	assert.Equal(t, 404, rw.Code)

}

func TestRegisterPixelVisit(t *testing.T) {
	config := svc.ParseFlags()
	resources := svc.NewResources(config)
	resources.ConnectRedis()
	manager := NewManager(resources)

	r := resources.Redis()
	defer r.Close()
	r.Do("FLUSHALL")

	uri := `/pixel?event=mail_hit` +
		`&tags=["contact_id:1","mailing_id:2"]`

	rw, req := GetPixelRWReq(uri)

	manager.HandlerRegisterPixelVisit(rw, req)

	assert.Equal(t, 200, rw.Code)

	jsd, err := redis.Bytes(r.Do("RPOP", "visits"))
	assert.Nil(t, err)

	v := &visit.Visit{RawVisit: jsd}
	err = v.Parse()
	assert.Nil(t, err)

	assert.Equal(t, "0d632ee9-121e-4e6e-8997-fa031996becd", v.Swuuid)
	assert.Equal(t, "1.2.3.4", v.Ip)

	uri = `/pixel?event=mail_hit1` +
		`&tags=["contact_id:1","mailing_id:2"]`

	rw, req = GetPixelRWReq(uri)

	manager.HandlerRegisterPixelVisit(rw, req)

	assert.Equal(t, 203, rw.Code)
	len, err := redis.Int(r.Do("LLEN", "visits"))
	assert.Nil(t, err)
	assert.Equal(t, 0, len)
}

func GetRWReq(uri, referer string) (*httptest.ResponseRecorder, *http.Request) {
	req, _ := http.NewRequest("GET", uri, nil)
	req.Header.Add("Referer", referer)
	req.Header.Add("X-Forwarded-For", "1.2.3.4")
	req.Header.Add("Cookie", "swuuid=0d632ee9-121e-4e6e-8997-fa031996becd")

	return httptest.NewRecorder(), req
}

func GetPixelRWReq(uri string) (*httptest.ResponseRecorder, *http.Request) {
	req, _ := http.NewRequest("GET", uri, nil)
	req.Header.Add("X-Forwarded-For", "1.2.3.4")
	req.Header.Add("Cookie", "swuuid=0d632ee9-121e-4e6e-8997-fa031996becd")

	return httptest.NewRecorder(), req
}
