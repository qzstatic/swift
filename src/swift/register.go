package main

import (
	"encoding/json"
	"errors"
	"net"
	"net/http"
	"net/url"
	"strings"

	"github.com/vedomosti/gol"
	"github.com/vedomosti/gore"
	"visit"
	"payment"
)

// Http handler for incomming visits
func (m *Manager) HandlerRegisterVisit(rw http.ResponseWriter, req *http.Request) {
	setDefaultHeaders(rw)

	if err := m.RegisterVisit(req); err != nil {
		gore.Appendf(err, "Request: %#v", req)
		gol.ErrorE(err)
		http.Error(rw, "", http.StatusNonAuthoritativeInfo)
	}

	setBody(rw, "")
}

// Http handler for incomming payments events
func (m *Manager) HandlerRegisterPaymentEvent(rw http.ResponseWriter, req *http.Request) {
	setDefaultHeaders(rw)

	if err := m.RegisterPayment(req); err != nil {
		gore.Appendf(err, "Request: %#v", req)
		gol.ErrorE(err)
		http.Error(rw, "", http.StatusNonAuthoritativeInfo)
	}

	setBody(rw, "")
}


// Http handler for redirect with statistics
func (m *Manager) HandlerRedirect(rw http.ResponseWriter, req *http.Request) {
	setDefaultHeaders(rw)

	link, err := formatUrl(req.FormValue("url"))
	if err != nil {
		gore.Appendf(err, "Request: %#v", req)
		gol.ErrorE(err)
	}
	if err != nil || link == "" {
		http.Error(rw, "", http.StatusNotFound)
		return
	}

	req.Header.Set("Referer", link)

	defer func() { http.Redirect(rw, req, link, http.StatusFound) }()

	if err := m.RegisterVisit(req); err != nil {
		gore.Appendf(err, "Request: %#v", req)
		gol.ErrorE(err)
		return
	}
}

// Http handler for visit, return transparent pixel
func (m *Manager) HandlerRegisterPixelVisit(rw http.ResponseWriter, req *http.Request) {
	setDefaultHeaders(rw)
	rw.Header().Set("Content-type", "image/gif")

	if req.Referer() == "" && isMailingEvent(req.FormValue("event")) {
		req.Header.Set("Referer", "mailing")
	}

	if err := m.RegisterVisit(req); err != nil {
		gore.Appendf(err, "Request: %#v", req)
		gol.ErrorE(err)
		http.Error(rw, "", http.StatusNonAuthoritativeInfo)
		return
	}

	setBody(rw, string(m.Resources.TransparentPixel))
}

// Register new visit for given request
func (m *Manager) RegisterPayment(req *http.Request) error {
	payment := payment.New(m.Resources.Redis())
	var err error

	payment.Sum 		= req.FormValue("sum")
	payment.Product 	= req.FormValue("product")
	payment.Namespace 	= req.FormValue("namespace")
	payment.Subproduct 	= req.FormValue("subproduct")
	payment.Period 		= req.FormValue("period")
	payment.Paymethod 	= req.FormValue("paymethod")
	payment.AccessToken 	= req.FormValue("access_token")
	payment.Msisdn 		= req.FormValue("msisdn")
	payment.UserId		= req.FormValue("user_id")
	payment.SessionId 	= req.FormValue("session_id")
	payment.PaymentId 	= req.FormValue("payment_id")
	payment.Status 		= req.FormValue("status")
	payment.Event 		= req.FormValue("event")
	payment.Document 	= req.FormValue("document_url")


	if err = payment.Push(); err != nil {
		return gore.Newf("Error PUSH payment %s", err)
	}

	if err = payment.Redis.Close(); err != nil {
		return gore.Newf("Error of release redis handler: %s", err)
	}

	if m.Resources.Config.Verbose {
		pretty, err := payment.Pretty()
		if err != nil {
			return err
		}
		gol.Info(string(pretty))
	}

	return nil
}


// Register new visit for given request
func (m *Manager) RegisterVisit(req *http.Request) error {
	visit := visit.New(m.Resources.Redis())
	var err error

	visit.RawUrl, _ = formatUrl(req.Referer())
	visit.RawReferer, _ = formatUrl(req.FormValue("referer"))

	if err = AppendTags(visit, req.FormValue("tags")); err != nil {
		return gore.Newf("Error fetch tags: %s", err)
	}

	visit.Ip = getIP(req)
	visit.UserAgent = req.Header.Get("User-Agent")
	visit.EventType = req.FormValue("event")
	visit.ViewType = req.FormValue("view")
	visit.Domain = req.FormValue("domain")
	visit.Path = req.FormValue("path")
	visit.AccessToken = req.FormValue("access_token")

	if err = visit.Push(); err != nil {
		return gore.Newf("Error PUSH visit %s", err)
	}

	if err = visit.Redis.Close(); err != nil {
		return gore.Newf("Error of release redis handler: %s", err)
	}

	if m.Resources.Config.Verbose {
		pretty, err := visit.Pretty()
		if err != nil {
			return err
		}
		gol.Info(string(pretty))
	}

	return nil
}

// Append to visit tags
func AppendTags(visit *visit.Visit, tags string) error {
	if tags == "" {
		tags = "[]"
	}
	return json.Unmarshal([]byte(tags), &visit.Tags)
}

// Check the length of the url, convert a domain to it ASCII form
func formatUrl(raw_url string) (s string, err error) {
	if len(raw_url) > 4096 {
		return "", errors.New("Invalid length url")
	}
	var u *url.URL

	if u, err = url.Parse(raw_url); err != nil {
		return "", err
	}

	u.RawQuery = u.Query().Encode()

	if u.Host, err = toASCII(u.Host); err != nil {
		return "", err
	}
	return u.String(), nil
}

func getIP(r *http.Request) string {
	if real_ip := r.Header.Get("X-Real-Ip"); len(real_ip) > 0 {
		return real_ip
	}
	if ipProxy := r.Header.Get("X-Forwarded-For"); len(ipProxy) > 0 {
		return strings.Split(ipProxy, ",")[0]
	}
	ip, _, _ := net.SplitHostPort(r.RemoteAddr)
	return ip
}

func isMailingEvent(event string) bool {
	switch event {
	case "mail_hit", "mail_banner_hit":
		return true
	}
	return false
}
