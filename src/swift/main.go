// Service for collect visits
package main

import (
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/vedomosti/gol"
	"svc"
)

var VERSION string = "v.1.0.0"

func main() {
	defer log.Println("Stop falco")

	svc.Multithreading()

	config := svc.ParseFlags()
	config.ShowVersion(VERSION)

	resources := svc.NewResources(config)
	err := resources.Setup()
	if err != nil {
		log.Println(err)
		return
	}

	manager := NewManager(resources)

	http.HandleFunc("/boom", manager.HandlerRegisterVisit)
	http.HandleFunc("/payments", manager.HandlerRegisterPaymentEvent)
	http.HandleFunc("/redirect", manager.HandlerRedirect)
	http.HandleFunc("/pixel", manager.HandlerRegisterPixelVisit)

	gol.Infof("Start falco server: %s", config.Falco)
	if err := http.ListenAndServe(config.Falco, nil); err != nil {
		log.Fatal(err)
	}
}

// Falco process manager
type Manager struct {
	Resources *svc.Resources
}

// Generate new example of falco manager
func NewManager(resources *svc.Resources) *Manager {
	return &Manager{
		Resources: resources,
	}
}

// Set default headers for cache control and content-type
func setDefaultHeaders(rw http.ResponseWriter) {
	rw.Header().Set("Content-type", "application/json")
	rw.Header().Set("Cache-Control", "no-cache, must-revalidate")
	rw.Header().Set("Pragma", "no-cache")
	rw.Header().Set("Expires", "Tue, 13 May 2014 12:00:00 GMT")
}

// Write body and set header content-length
func setBody(rw http.ResponseWriter, body string) {
	rw.Header().Set("Content-Length", strconv.Itoa(len(body)))
	fmt.Fprint(rw, body)
}
