package payment

import (
	"encoding/json"
	"time"

	"github.com/garyburd/redigo/redis"
)

// Payment struct
type Payment struct {
	PayStatus         	string     `json:"pay_status"`
	Sum			string	   `json:"sum"`
	Product			string	   `json:"product"`
	Namespace		string	   `json:"namespace"`
	Subproduct		string	   `json:"subproduct"`
	Period			string	   `json:"period"`
	Paymethod		string	   `json:"paymethod"`
	AccessToken 		string     `json:"access_token"`
	Msisdn			string	   `json:"msisdn"`
	UserId			string	   `json:"user_id"`
	SessionId		string	   `json:"session_id"`
	Event    		string     `json:"event"`
	PaymentId		string	   `json:"payment_id"`
	Status			string	   `json:"status"`
	Time        		time.Time  `json:"-"`
	RawPayment    		[]byte     `json:"-"`
	Redis       		redis.Conn `json:"-"`
	UnixTime    		int64      `json:"time"`
  Document    string       `json:"document_url"`
}

const (
	QPayments string = "payments"
)

// Return new exemplar of Payment
func New(r redis.Conn) *Payment {
	return &Payment{
		Redis:    r,
		UnixTime: time.Now().Unix(),
	}
}

// Push new Payment to Payments queue
func (v *Payment) Push() (err error) {

	v.RawPayment, err = json.Marshal(v)
	if err != nil {
		return err
	}

	_, err = v.Redis.Do("RPUSH", QPayments, v.RawPayment)
	if err != nil {
		return err
	}

	return nil
}

// Pretty json for Payment
func (v *Payment) Pretty() ([]byte, error) {
	return json.MarshalIndent(v, "", "\t")
}
