package svc

import (
	"encoding/base64"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"runtime"
	"strconv"
	"strings"
	"syscall"
	"time"

	"github.com/garyburd/redigo/redis"
	"github.com/vedomosti/gol"
)

// Represent all configurations of program
type Config struct {
	LogFile  string
	LogLevel gol.Level
	Verbose  bool
	Redis    string
	Version  bool
	Restore  bool
	Falco    string
}

var (
	// Determine the log file.
	// By default, the log put into a temporary directory
	logfileFlag = flag.String("log", "", "The full path to the log file")

	// Log level
	logLevelFlag = flag.String("log_level", "INFO", "LogLevel: DEBUG|INFO|WARN|ERROR|FATAL|PANIC")

	// Verbose logging
	verboseFlag = flag.Bool("verbose", false, "Verbose logging")

	// Show version
	versionFlag = flag.Bool("version", false, "Show version")

	// Host and port server redis
	redisFlag = flag.String("redis", "localhost:6379/0", "host:port for redis server.")

	// Restore visits from
	restoreFlag = flag.Bool("restore", false, "Restore visits from processing queue")

	// Host and port falco server
	falcoFlag = flag.String("falco", "localhost:9034", "host:port for falco server.")
)

const pixel_base64 = "R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"

// Parse command line arguments and return Config struct
func ParseFlags() *Config {
	flag.Parse()

	conf := &Config{
		LogFile:  *logfileFlag,
		LogLevel: gol.LogLevel(*logLevelFlag),
		Verbose:  *verboseFlag,
		Version:  *versionFlag,
		Redis:    *redisFlag,
		Restore:  *restoreFlag,
		Falco:    *falcoFlag,
	}

	return conf
}

// Show version of app if require
func (c *Config) ShowVersion(v string) {
	if c.Version {
		fmt.Println(v)
		os.Exit(0)
	}
}

// Resources manager
type Resources struct {
	Config    *Config
	RedisPool *redis.Pool
	// Store all prepared statements
	TransparentPixel []byte
}

// New exemplar of Resources
func NewResources(c *Config) *Resources {
	pixel, _ := base64.StdEncoding.DecodeString(pixel_base64)
	return &Resources{
		Config:           c,
		TransparentPixel: pixel,
	}
}

// Return new redis connection from pool
func (r *Resources) Redis() redis.Conn {
	return r.RedisPool.Get()
}

// Setup and connect all resources
func (r *Resources) Setup() error {
	var err error

	if err = r.SetupLogger(); err != nil {
		return err
	}

	if _, err = r.ConnectRedis(); err != nil {
		return err
	}

	return nil
}

// Configure the logger
func (r *Resources) SetupLogger() error {
	var dest *os.File
	var err error

	if r.Config.LogFile == "" {
		dest = os.Stdout
	} else {
		// open the file with the right flags
		dest, err = os.OpenFile(r.Config.LogFile, os.O_RDWR|os.O_APPEND|os.O_CREATE, 0666)
		if err != nil {
			return err
		}
	}

	log.SetOutput(io.Writer(dest))
	log.SetPrefix(fmt.Sprintf("%5d | ", syscall.Getpid()))
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	gol.SetOutput(io.Writer(dest))
	gol.SetLevel(r.Config.LogLevel)

	return nil
}

// Establishing a connection with redis
func (r *Resources) ConnectRedis() (*redis.Pool, error) {
	els := strings.Split(r.Config.Redis, "/")
	db := 0
	if len(els) == 2 {
		db, _ = strconv.Atoi(els[1])
	}

	r.RedisPool = &redis.Pool{
		MaxIdle:     100,
		IdleTimeout: 240 * time.Second,
		Dial: func() (redis.Conn, error) {
			conn, err := redis.Dial("tcp", els[0])
			if err != nil {
				return nil, err
			}
			_, err = conn.Do("SELECT", db)
			if err != nil {
				return nil, err
			}
			return conn, err
		},
		TestOnBorrow: func(conn redis.Conn, t time.Time) error {
			_, err := conn.Do("Ping")
			return err
		},
	}

	_, err := r.RedisPool.Get().Do("Ping")

	if err != nil {
		return nil, fmt.Errorf("Error redis connection: %s", err)
	}

	return r.RedisPool, nil
}

// Use all CPU for multiprocessing
func Multithreading() {
	runtime.GOMAXPROCS(runtime.NumCPU())
}
