package visit

import (
	"encoding/json"
	"fmt"
	"net/url"
	"time"
	"unicode/utf8"

	"github.com/garyburd/redigo/redis"
)

// Visit struct
type Visit struct {
	Url         *url.URL   `json:"-"`
	RawUrl      string     `json:"url"`
	Time        time.Time  `json:"-"`
	UnixTime    int64      `json:"time"`
	Tags        []string   `json:"tags"`
	EventType   string     `json:"event"`
	ViewType    string     `json:"view"`
	Domain      string     `json:"domain"`
	Path        string     `json:"path"`
	Ip          string     `json:"ip"`
	UserAgent   string     `json:"user_agent"`
	Referer     *url.URL   `json:"-"`
	RawReferer  string     `json:"referer"`
	RawVisit    []byte     `json:"-"`
	Redis       redis.Conn `json:"-"`
	AccessToken string     `json:"access_token"`
}

const (
	QVisits string = "visits"
)

// Return new exemplar of visit
func New(r redis.Conn) *Visit {
	return &Visit{
		Redis:    r,
		UnixTime: time.Now().Unix(),
		Tags:     make([]string, 0),
	}
}

// Push new visit to visits queue
func (v *Visit) Push() (err error) {

	v.RawVisit, err = json.Marshal(v)
	if err != nil {
		return err
	}

	_, err = v.Redis.Do("RPUSH", QVisits, v.RawVisit)
	if err != nil {
		return err
	}

	return nil
}

// Parse raw visit
func (v *Visit) Parse() error {
	var err error

	if err = json.Unmarshal(v.RawVisit, v); err != nil {
		return fmt.Errorf("Error parse json: %s\n%s", err, string(v.RawVisit))
	}

	if v.Url, err = parseUrl(v.RawUrl); err != nil {
		return fmt.Errorf("Error parse url: %s\n%s", err, v.RawUrl)
	}

	if v.Referer, err = parseUrl(v.RawReferer); err != nil {
		return fmt.Errorf("Error parse referer: %s\n%s", err, v.RawReferer)
	}

	v.EventType = preparedEventType(v.EventType)
	v.ViewType = preparedViewType(v.ViewType)

	v.Time = time.Unix(v.UnixTime, 0).UTC().Truncate(time.Minute)

	return nil
}

// Pretty json for visit
func (v *Visit) Pretty() ([]byte, error) {
	return json.MarshalIndent(v, "", "\t")
}

// Check valid of url and parse it
func parseUrl(raw_url string) (u *url.URL, err error) {
	// Check valid utf8 for Url
	if !utf8.ValidString(raw_url) {
		return nil, fmt.Errorf("Invalid url, not utf8: %v", raw_url)
	}

	if u, err = url.Parse(raw_url); err != nil {
		return nil, err
	}
	return u, nil
}

// Set default value event type if needed
func preparedEventType(et string) string {
	if et == "" {
		et = "hit"
	}
	return et
}

// Set default value view type if needed
func preparedViewType(vt string) string {
	if vt == "" {
		vt = "desktop"
	}
	return vt
}
