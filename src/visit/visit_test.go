package visit

import (
	"testing"

	"github.com/garyburd/redigo/redis"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"github.com/vedomosti/swift/svc"
)

var RawVisit string = `{
		"url":"http://www.vedomosti.ru/news/russia/foobar/?search=mango",
		"tags": ["type:news", "rubric:russia", "rubric:media"],
		"user_agent": "Firefox",
		"swuuid": "98A7F0CA-2261-45DA-9E53-E45B07D69FAE",
		"referer": "http://news.yandex.ru/foo/bar",
		"time": 1401962583,
		"ip": "91.230.26.225",
		"event": "",
		"view": ""
	}`

type VisitSuite struct {
	suite.Suite
	Resources *svc.Resources
}

func (suite *VisitSuite) SetupSuite() {
	config := svc.ParseFlags()
	suite.Resources = svc.NewResources(config)

	if config.Verbose {
		err := suite.Resources.SetupLogger()
		assert.Nil(suite.T(), err)
	}

	suite.Resources.ConnectRedis()
	suite.Resources.Redis().Do("FLUSHALL")
}

func TestVisitSuite(t *testing.T) {
	suite.Run(t, new(VisitSuite))
}

func (suite *VisitSuite) TestExtract() {
	r := suite.Resources.Redis()
	r.Do("RPUSH", QVisits, RawVisit)

	v, err := Extract(r)
	assert.Nil(suite.T(), err)
	assert.Equal(suite.T(), v.Url.Host, "www.vedomosti.ru")

	dv, err := redis.Int(r.Do("LREM", QVisitsProcessing, 1, RawVisit))
	assert.Equal(suite.T(), 1, dv)
	assert.Nil(suite.T(), err)
}

func (suite *VisitSuite) TestRemoveProcessed() {
	visit := &Visit{Redis: suite.Resources.Redis(), RawVisit: []byte("{foo:bar}")}
	visit.Redis.Do("RPUSH", QVisitsProcessing, visit.RawVisit)
	err := visit.RemoveProcessed()
	assert.Nil(suite.T(), err)
}

func (suite *VisitSuite) TestRestoreProcessing() {
	r := suite.Resources.Redis()
	r.Do("FLUSHALL")
	r.Do("RPUSH", QVisitsProcessing, "{1}")
	r.Do("RPUSH", QVisitsProcessing, "{2}")
	r.Do("RPUSH", QVisitsProcessing, "{3}")

	cnt, err := RestoreProcessing(r)
	assert.Nil(suite.T(), err)
	assert.Equal(suite.T(), 3, cnt)
	dv, err := redis.Int(r.Do("LREM", QVisitsProcessing, 1, "{1}"))
	assert.Equal(suite.T(), 0, dv)
	dv, err = redis.Int(r.Do("LREM", QVisits, 1, "{1}"))
	assert.Equal(suite.T(), 1, dv)
}

func (suite *VisitSuite) TestPush() {
	visit := New(suite.Resources.Redis())
	visit.RawUrl = "http://news.yandex.ru"
	err := visit.Push()
	assert.Nil(suite.T(), err)
	dv, err := redis.Int(visit.Redis.Do("LREM", QVisits, 1, visit.RawVisit))
	assert.Equal(suite.T(), 1, dv)
}
